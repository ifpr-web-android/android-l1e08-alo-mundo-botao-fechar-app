package com.example.everaldo.l1e8alomundobotaofecharapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String NOME_PESSOA = "NOME_PESSOA";
    private static final int REQUEST_CUMPRIMENTA = 1;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void surpresa(View view){
        String nomePessoa = ((EditText)findViewById(R.id.nomePessoa)).getText().toString();
        Intent intent = new Intent(this, CumprimentaActivity.class);
        intent.putExtra(NOME_PESSOA, nomePessoa);
        startActivityForResult(intent, REQUEST_CUMPRIMENTA);
    }


    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

        if(requestCode == REQUEST_CUMPRIMENTA && resultCode == RESULT_OK){
            Log.d(TAG, data.getStringExtra(CumprimentaActivity.RESULTADO));
        }
        finish();
    }

}
