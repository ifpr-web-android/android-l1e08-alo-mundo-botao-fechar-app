package com.example.everaldo.l1e8alomundobotaofecharapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CumprimentaActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "CumprimentaActivity";
    public static final String RESULTADO = "RESULTADO" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cumprimenta);

        Intent intent = getIntent();
        String nomePessoa =  intent.getStringExtra(MainActivity.NOME_PESSOA);

        TextView cumprimenta = (TextView) findViewById(R.id.cumprimentaTextView);
        cumprimenta.setText(alo(nomePessoa));

        Button ate_logo_botao = (Button) findViewById(R.id.ate_logo_botao);

        ate_logo_botao.setOnClickListener(this);


    }

    @Override
    protected void onDestroy(){
        Log.d(TAG, "Essa Atividade está sendo finalizada");
        super.onDestroy();
    }


    private String alo(String nomePessoa){
        return "Alô, " + nomePessoa + "!";
    }

    @Override
    public void onClick(View view) {
        Intent data = new Intent();
        data.putExtra(RESULTADO, "Terminamos por aqui, doc!");
        setResult(RESULT_OK, data);
        finish();
    }
}
